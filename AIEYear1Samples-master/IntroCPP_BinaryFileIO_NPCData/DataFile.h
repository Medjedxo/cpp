#pragma once

#include "raylib.h"
#include <string>
#include <vector>
#include <istream>

using namespace std;

class DataFile
{
public:

	DataFile();
	~DataFile();

	struct Record 
	{
		Image image;
		string name;
		int age;
	};

	void AddRecord(string imageFilename, string name, int age);
	Record* GetRecord(int index);

	int GetRecordCount() { return recordCount; };

	void Save(string filename);
	void Load(string filename);

private:
	string filename;
	int recordCount { 0 };
	Record currentRecord;
	int currentRecordIdx = 0;

	void Clear();
};

