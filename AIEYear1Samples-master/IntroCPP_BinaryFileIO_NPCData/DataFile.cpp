#include "DataFile.h"
#include <fstream>
using namespace std;

DataFile::DataFile()
{
	recordCount = 0;
}

DataFile::~DataFile()
{
	Clear();
}

void DataFile::AddRecord(string imageFilename, string name, int age)
{
	fstream outfile("npc_data.dat", ios::binary | ios::app | ios::out);

	outfile.read((char*)&recordCount, sizeof(int));
	recordCount++;

	outfile.seekg(0, ios::beg);
	outfile.write((char*)&recordCount, sizeof(int));
	outfile.seekg(0, ios::end);
	
	Image i = LoadImage(imageFilename.c_str());

	Record* r = new Record;
	r->image = i;
	r->name = name;
	r->age = age;

	Color* imgdata = GetImageData(r->image);
	int imageSize = sizeof(Color) * r->image.width * r->image.height;
	int nameSize = r->name.length();
	int ageSize = sizeof(int);

	outfile.write((char*)&r->image.width, sizeof(int));
	outfile.write((char*)&r->image.height, sizeof(int));

	outfile.write((char*)&nameSize, sizeof(int));
	outfile.write((char*)&ageSize, sizeof(int));

	outfile.write((char*)imgdata, imageSize);
	outfile.write((char*)r->name.c_str(), nameSize);
	outfile.write((char*)&r->age, ageSize);
	
	outfile.close();
}

DataFile::Record* DataFile::GetRecord(int index)
{
	ifstream infile("npc_data.dat", ios::binary);
	
	infile.read((char*)&recordCount, sizeof(int));

	for (int i = 0; i <= index; i++)
	{
		int nameSize = 0;
		int ageSize = 0;
		int width = 0, height = 0, format = 0, imageSize = 0;

		infile.read((char*)&width, sizeof(int));
		infile.read((char*)&height, sizeof(int));

		imageSize = sizeof(Color) * width * height;

		infile.read((char*)&nameSize, sizeof(int));
		infile.read((char*)&ageSize, sizeof(int));

		int fileSize = ageSize + nameSize + imageSize;
		infile.seekg(fileSize, ios::cur);
		
		char* imgdata = new char[imageSize];
		infile.read(imgdata, imageSize);

		Image img = LoadImageEx((Color*)imgdata, width, height);
		char* name = new char[nameSize];
		int age = 0;
				
		infile.read((char*)name, nameSize);
		infile.read((char*)&age, ageSize);

		Record* r = new Record();
		r->image = img;
		r->name = string(name);
		r->age = age;
		
		//return DataFile::records[index];
		return r;
	}
    infile.close();
}

void DataFile::Save(string filename)
{
	fstream outfile("npc_data.dat", ios::binary | ios::out);

	//int recordCount = records.size();
	outfile.write((char*)&recordCount, sizeof(int));

	for (int i = 0; i < recordCount; i++)
	{

	}


	/*for (int i = 0; i < recordCount; i++)
	{		
		Color* imgdata = GetImageData(records[i]->image);
				
		int imageSize = sizeof(Color) * records[i]->image.width * records[i]->image.height;
		int nameSize = records[i]->name.length();
		int ageSize = sizeof(int);

		outfile.write((char*)&records[i]->image.width, sizeof(int));
		outfile.write((char*)&records[i]->image.height, sizeof(int));
		
		outfile.write((char*)&nameSize, sizeof(int));
		outfile.write((char*)&ageSize, sizeof(int));

		outfile.write((char*)imgdata, imageSize);
		outfile.write((char*)records[i]->name.c_str(), nameSize);
		outfile.write((char*)&records[i]->age, ageSize);
	}*/

	outfile.close();
}

void DataFile::Load(string filename)
{
	Clear();

	ifstream infile(filename, ios::binary);
	infile.read((char*)&recordCount, sizeof(int));
	this->filename = filename;

	//for (int i = 0; i < recordCount; i++)
	//{		
	//	int nameSize = 0;
	//	int ageSize = 0;
	//	int width = 0, height = 0, format = 0, imageSize = 0;

	//	infile.read((char*)&width, sizeof(int));
	//	infile.read((char*)&height, sizeof(int));

	//	imageSize = sizeof(Color) * width * height;

	//	infile.read((char*)&nameSize, sizeof(int));
	//	infile.read((char*)&ageSize, sizeof(int));

	//	char* imgdata = new char[imageSize];
	//	infile.read(imgdata, imageSize);

	//	Image img = LoadImageEx((Color*)imgdata, width, height);
	//	char* name = new char[nameSize];
	//	int age = 0;
	//			
	//	infile.read((char*)name, nameSize);
	//	infile.read((char*)&age, ageSize);

	//	Record* r = new Record();
	//	r->image = img;
	//	r->name = string(name);
	//	r->age = age;
	//	records.push_back(r);

	//	delete [] imgdata;
	//	delete [] name;
	//}

	infile.close();
}

void DataFile::Clear()
{
	//for (int i = 0; i < records.size(); i++)
	//{
	//	delete records[i];
	//}
	//records.clear();
	recordCount = 0;
}