#pragma once
#include "Game.h"
#include "Leaderboard.h"
#include <string>


class PlayerDatabase :
		public Game
{
public:
	const char* LeaderboardFileName = "leaderboard.dat";

protected:
	// Game Override
	virtual void Init() override;
	virtual void Shutdown() override;
	virtual bool IsGameRuning() override;

	virtual void Update() override;
	virtual void Draw() override;

private:
	bool isGameRuning = true;

	Leaderboard leaderboard;

	string getMenuOption();;
	void displayMenu();
	void addNewPlayer();
	void modifyPlayerByIndex();
	void modifyPlayerByName();
	void hackTheLeaderboardfile();
};

