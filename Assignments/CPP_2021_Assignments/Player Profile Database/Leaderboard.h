#pragma once
#include "Player.h"
#include <memory>
#include <string>

using namespace std;

// This class manages the dynamic array 
class Leaderboard
{
public:
	const unsigned int MaxLeaderboardSize = 1000;

	Leaderboard(unsigned int _maxPlayers = 10);
	~Leaderboard();

	bool IsEmpty() const { return (playersInUse == 0); }
	bool IsFull() const { return (playersInUse >= maxPlayers); }

	unsigned int PlayersInUse() const { return playersInUse; }
	unsigned int MaxPlayers() const { return maxPlayers; }

	unsigned int Size() const { return playersInUse; }
	unsigned int Capacity() const { return maxPlayers; }

	Player& GetPlayer(unsigned int pos) const;
	Player& operator[](unsigned int pos) const;

	void Draw();
	void AddPlayer(const string& name, unsigned int score);
	void AddPlayer(const Player& player);
	void Clear();
	void SortByHighScore();
	void SortByName();

	// Binary search trough the collection. 'true' on success and 'false' if not found.
	// If true, also updates to reflect the position. 
	bool Search(const string& name, unsigned int& posFound);
	
	bool Load(const char* filename);
	bool Save(const char* filename);
	


private:
	Player* playerList = nullptr;

	unsigned int maxPlayers = 0;
	unsigned int playersInUse = 0;                                                                       
};

