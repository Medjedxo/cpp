#include "PlayerDatabase.h"
#include <iostream>
#include <stdlib.h>
#include <string>
#include <algorithm>
#include "ioUtilities.h"
#include <fstream>

using namespace std;

void PlayerDatabase::Init()
{
	// Load leaderboard from file

	try 
	{
		leaderboard.Load(LeaderboardFileName);

		leaderboard.SortByHighScore();
	}
	catch (exception& err)
	{
		cout << "Error:" << err.what() << endl;
	}
	
}

void PlayerDatabase::Shutdown()
{
	cout << "Do you want to save leaderboard on exit (y/n)?";

	CinClear();
	unsigned char c;
	cin >> c;

	if (c == 'y' || c == 'Y')
	leaderboard.Save(LeaderboardFileName);
}

bool PlayerDatabase::IsGameRuning()
{
	return isGameRuning;
}

void PlayerDatabase::Update()
{
	displayMenu();
	string menuOption = getMenuOption();

	if (menuOption == "a")
	{
		addNewPlayer();
	}
	else if (menuOption == "m")
	{
		modifyPlayerByName();
	}
	else if (menuOption == "n")
	{
		leaderboard.SortByName();
	}
	else if (menuOption == "l")
	{
		if (!leaderboard.Load(LeaderboardFileName))
		{
			cerr << "Error loading file: " << LeaderboardFileName << endl;
			CinClear();
			int ch = getchar();
		}
		else
		{
			leaderboard.SortByHighScore();
		}
	}
	else if (menuOption == "s")
	{
		if (!leaderboard.Save(LeaderboardFileName))
		{
			cerr << "Error lsaving file: " << LeaderboardFileName << endl;
			CinClear();
			int ch = getchar();
		}
	}
	else if (menuOption == "h")
	{
		hackTheLeaderboardfile();
	}
	else if (menuOption == "c")
	{
		leaderboard.Clear();
	}
	else if (menuOption == "q")
	{
		isGameRuning = false;
	}
}

void PlayerDatabase::Draw()
{
	system("cls");
	leaderboard.Draw();
}

void PlayerDatabase::displayMenu()
{
	cout << endl << "--- Menu ---" << endl;
	cout << "A)dd player" << endl;
	cout << "M)odify player" << endl;
	cout << "N)ame sort" << endl;
	cout << "C)lear leaderboard" << endl;
	cout << "L)oad filedata" << endl;
	cout << "S)ave filedata" << endl;
	cout << "H)ack filedata" << endl;
	cout << "Q)uit" << endl;
	cout << "------------" << endl;
	cout << "> ";
}

string PlayerDatabase::getMenuOption()
{
	string userInput;
	CinClear();
	cin >> userInput;

	transform(userInput.begin(), userInput.end(), userInput.begin(), ::tolower); //Testing if this function transforms input into lower case
	return userInput;
}

void PlayerDatabase::addNewPlayer()
{
	if (!leaderboard.IsFull())
	{
		Player p;
		if (p.AskUser())
		{
			leaderboard.AddPlayer(p);

			leaderboard.SortByHighScore();
		}	
	}
	else
	{
		cout << " Leaderboard is full" << endl;
		int ch = getchar();
	}
}

void PlayerDatabase::modifyPlayerByIndex()
{
	CinClear();
	cout << "Enter  player position to modify> ";
	unsigned int pos;
	cin >> pos;

	if (pos < leaderboard.PlayersInUse())
	{
		leaderboard[pos].AskUser();

		leaderboard.SortByHighScore();
	}
}

void PlayerDatabase::modifyPlayerByName()
{
	CinClear();
	cout << "Enter name of player to modify> ";
	string name;
	cin >> name;

	unsigned int pos = 0;

	if (leaderboard.Search(name, pos))
	{
		leaderboard[pos].AskUser();

		leaderboard.SortByHighScore();
	}
}

void PlayerDatabase::hackTheLeaderboardfile()
{
	CinClear();
	cout << "Enter name of player to modify> ";
	string name;
	cin >> name;

	

	unsigned int pos = 0;

	fstream file(LeaderboardFileName, ios_base::in | ios_base::out | ios_base::binary);
	if (file.good())
	{
		file.seekg(sizeof(unsigned int) * 2);

		Player player;
		while (!file.eof())
		{
			file.read((char*)&player, sizeof(Player));
			if (name == player.GetName())
			{
				cout << "Found player:" << name << endl;
				player.AskUser();


				const int sizeOfPlayer = sizeof(Player);
				file.seekg(-sizeOfPlayer, ios::cur);

				file.write((const char*)&player, sizeof(Player));


				file.close();
				int ch = getchar();
				return;
			}

		}

		cout << "Did not find player:" << name << endl;
		file.close();
	}
	else
	{
		cerr << "Failed to load file : " << LeaderboardFileName << endl;
	}
	CinClear();
	int ch = getchar();
}




