#include "Leaderboard.h"
#include <iostream>
#include <algorithm>
#include <fstream>
#include "ioUtilities.h"

using namespace std;

Leaderboard::Leaderboard(unsigned int _maxPlayers)
	: maxPlayers(_maxPlayers), playersInUse(0)
{
	if (maxPlayers == 0)
		maxPlayers = 10;

	if (maxPlayers > 100)
		maxPlayers = 100;
	
	playerList = new Player[maxPlayers];
}

Leaderboard::~Leaderboard()
{
	delete[] playerList;
}


void Leaderboard::Draw()
{
	cout << "================================" << endl;
	cout << "==       Leaderboard          ==" << endl;
	cout << "================================" << endl;

	if (!IsEmpty())
	{
		for (unsigned int i = 0; i < playersInUse; i++)
		{
			cout << i << ".) ";
			playerList[i].Draw();
		}
	}
	else
		cout << "... Empty ..." << endl;
	
}

void Leaderboard::AddPlayer(const string& name, unsigned int score)
{
	AddPlayer(Player(score, name.c_str()));
}

void Leaderboard::AddPlayer(const Player& player)
{
	if (playersInUse < maxPlayers)
	{
		playerList[playersInUse] = player;
		playersInUse++;
	}
	else
	{
		throw exception("Out of bounds. Leaderboard full.");
	}
}

void Leaderboard::Clear()
{
	playersInUse = 0;
}

bool comparePlayerScores(const Player& lhs, const Player& rhs)
{
	return lhs.GetHighScore() > rhs.GetHighScore();
}

void Leaderboard::SortByHighScore()
{
	sort(playerList, playerList + playersInUse, comparePlayerScores);
}

bool comparePlayerNames(const Player& lhs, const Player& rhs)
{
	return (strcmp(lhs.GetName(), rhs.GetName()) < 0);
}

void Leaderboard::SortByName()
{
	sort(playerList, playerList + playersInUse, comparePlayerNames);
}

bool Leaderboard::Search(const string& name, unsigned int& posFound)
{
	SortByName();

	unsigned int l = 0;
	unsigned int r = playersInUse - 1;
	unsigned int m;

	while (l <= r)
	{
		m = (l + r) / 2;

		if (name == playerList[m].GetName())
		{
			posFound = m;
			return true;
		}

		else if (name < playerList[m].GetName())
		{
			r = m - 1;
		}

		else if (name > playerList[m].GetName())
		{
			l = m + 1;
		}
	}

	return false;
}

bool Leaderboard::Load(const char* filename)
{
	fstream fin(filename, ios_base::in | ios_base::binary);
	if (fin.good())
	{
		unsigned int maxPlayers;
		fin.read((char*)&maxPlayers, sizeof(unsigned int));

		if (maxPlayers > MaxLeaderboardSize)
		{
			cerr << "Leaderboard:: Invalid file format. maxPlayer is too large" << endl;;
			fin.close();
			return false;
		}

		if (this->maxPlayers != maxPlayers)
		{
			this->maxPlayers = maxPlayers;
			delete[] this->playerList;
			this->playerList = new Player[maxPlayers];
		}

		unsigned int playersInUse ;
		fin.read((char*)&playersInUse, sizeof(unsigned int));
		
		if (playersInUse > maxPlayers)
		{
			cerr << "Leaderboard:: Invalid file format. playerInUse is too large" << endl;;
			fin.close();
			return false;
		}
		
		this->playersInUse = playersInUse;


		fin.read((char*)playerList, streamsize(playersInUse) * sizeof(Player));
		
		fin.close();
		return true;
	}

	return false;
}

bool Leaderboard::Save(const char* filename)
{
	 ofstream fout(filename, ios_base::out | ios_base::binary);

	 if (fout.good())
	 {
		 fout.write((const char*)&maxPlayers, sizeof(unsigned int));

		 fout.write((const char*)&playersInUse, sizeof(unsigned int));
		 
		 fout.write((const char*)playerList, streamsize(playersInUse) * sizeof(Player));

		 fout.close();
		 return true;
	 }


	return false;
}


Player& Leaderboard::GetPlayer(unsigned int pos) const
{
	if (pos > playersInUse)
	{
		throw out_of_range("Out of bounds.");
	}

	return playerList[pos];
}

Player& Leaderboard::operator[](unsigned int pos) const
{
	return GetPlayer(pos);
}
