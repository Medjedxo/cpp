#include "DataFile.h"
#include <fstream>
#include <iostream>
using namespace std;

DataFile::DataFile()
{
	recordCount = 0;
}

DataFile::~DataFile()
{
	Clear();
}

void DataFile::AddRecord(string imageFilename, string name, int age)
{
	fstream outfile("npc_data.dat", ios::binary | ios::app | ios::out);

	outfile.read((char*)&recordCount, sizeof(int));
	recordCount++;

	// Seek the starting position inside the file to where we add the file
	outfile.seekg(0, ios::beg);
	outfile.write((char*)&recordCount, sizeof(int));
	outfile.seekg(0, ios::end);

	// Load the new record data
	Image i = LoadImage(imageFilename.c_str());

	Record* r = new Record;
	r->image = i;
	r->name = name;
	r->age = age;

	Color* imgdata = GetImageData(r->image);
	int imageSize = sizeof(Color) * r->image.width * r->image.height;
	int nameSize = r->name.length();
	int ageSize = sizeof(int);

	// Write the new record to the file and closes it
	outfile.write((char*)&r->image.width, sizeof(int));
	outfile.write((char*)&r->image.height, sizeof(int));

	outfile.write((char*)&nameSize, sizeof(int));
	outfile.write((char*)&ageSize, sizeof(int));

	outfile.write((char*)imgdata, imageSize);
	outfile.write((char*)r->name.c_str(), nameSize);
	outfile.write((char*)&r->age, ageSize);

	outfile.close();
}

DataFile::Record* DataFile::GetRecord(int index)
{
	ifstream infile(filename, ios::binary);

	recordCount = 0;
	infile.read((char*)&recordCount, sizeof(int));

	currentRecord = 0;
	currentRecord = index;

	// Goes trough out the file till it hits the given position
	for (int i = 0; i <= currentRecord; i++)
	{
		int nameSize = 0;
		int ageSize = 0;
		int width = 0, height = 0, format = 0, imageSize = 0;

		// Get the size of the data inside
		infile.read((char*)&width, sizeof(int));
		infile.read((char*)&height, sizeof(int));

		imageSize = sizeof(Color) * width * height;

		infile.read((char*)&nameSize, sizeof(int));
		infile.read((char*)&ageSize, sizeof(int));

		if (i < currentRecord)
		{
			infile.seekg(streamsize(imageSize) + streamsize(nameSize) + streamsize(ageSize), ios::cur);
		}
		else
		{
			// Reads the data in the position
			char* imgdata = new char[imageSize];
			infile.read(imgdata, imageSize);

			Image img = LoadImageEx((Color*)imgdata, width, height);
			char* name = new char[nameSize + 1];
			int age = 0;

			infile.read((char*)name, nameSize);
			name[nameSize] = '\0';
			infile.read((char*)&age, ageSize);

			// Sets the data into the record to hold it
			record.image = img;
			record.name = string(name);
			record.age = age;

			// Deletes the data after it's done to avoid memory leak
			delete[] imgdata;
			delete[] name;

			// Returns the record holding the data and closes file
			infile.close();
			return &record;
		}
	}
	return nullptr;
}

void DataFile::Save(string filename)
{
	fstream outfile(filename, ios_base::out | ios_base::binary);

	// Opens the file and writes all current record into it, closes when finished
	if (outfile.good())
	{
		outfile.write((char*)&recordCount, sizeof(int));

		outfile.write((const char*)&currentRecord, sizeof(unsigned int));

		outfile.write((const char*)currentRecord, streamsize(recordCount) * sizeof(Record));

		outfile.close();
	}
}

void DataFile::Load(string filename)
{
	// Clear buffer 
	Clear();

	ifstream infile(filename, ios::binary);

	recordCount = 0;
	infile.read((char*)&recordCount, sizeof(int));
	this->filename = filename;

	// Go trough every record and loadit
	for (int i = 0; i < currentRecord; i++)
	{
		int nameSize = 0;
		int ageSize = 0;
		int width = 0, height = 0, format = 0, imageSize = 0;

		infile.read((char*)&width, sizeof(int));
		infile.read((char*)&height, sizeof(int));

		imageSize = sizeof(Color) * width * height;

		infile.read((char*)&nameSize, sizeof(int));
		infile.read((char*)&ageSize, sizeof(int));

		char* imgdata = new char[imageSize];
		infile.read(imgdata, imageSize);

		Image img = LoadImageEx((Color*)imgdata, width, height);
		char* name = new char[nameSize];
		int age = 0;

		infile.read((char*)name, nameSize);
		infile.read((char*)&age, ageSize);
	}
	infile.close();
}

void DataFile::Clear()
{
	filename = "";
	recordCount = 0;
}