#include <iostream>
#include <conio.h>

using namespace std;


const unsigned int BoardSize = 3;
int turn = 1; //1- player1 | 0-player2
char mark = 'O'; //O-Player1 | X-Player2
int input;

char Board[BoardSize][BoardSize] = {	' ',' ',' ',
										' ',' ',' ',
										' ',' ',' ' };


void printInputMatrix()
{
	cout << endl << endl << "INPUT MATRIX" << endl;
	cout << " 7 | 8 | 9 " << endl;
	cout << "-----------" << endl;
	cout << " 4 | 5 | 6 " << endl;
	cout << "-----------" << endl;
	cout << " 1 | 2 | 3 " << endl;
}

void printBoard()
{
	cout << " "<< Board[2][0] << " | " << Board[2][1] << " | " << Board[2][2] <<" " <<endl;
	cout << "------------" << endl;
	cout << " " << Board[1][0] << " | " << Board[1][1] << " | " << Board[1][2] << " " << endl;
	cout << "----------- " << endl;
	cout << " " << Board[0][0] << " | " << Board[0][1] << " | " << Board[0][2] << " " << endl;
}

int addMark()
{
	for (int i = 0, col = 1; i < BoardSize; i++)
	{
		for (int row = 0; row < BoardSize; row++, col++)
		{
			if (col == input)
			{
				if (Board[i][row] == ' ')
				{
					Board[i][row] = mark;
					return 1;
				}
				else
				{
					cout << "Invalid Input";
					return 0;
				}
			}
		}
	}
}

int check()
{
	// checking rows
	if (Board[0][0] == mark && Board[0][1] == mark && Board[0][2] == mark)
		return 1;
	if (Board[1][0] == mark && Board[1][1] == mark && Board[1][2] == mark)
		return 1;
	if (Board[2][0] == mark && Board[2][1] == mark && Board[2][2] == mark)
		return 1;
	// cheking colums
	if (Board[0][0] == mark && Board[1][0] == mark && Board[2][0] == mark)
		return 1;
	if (Board[0][1] == mark && Board[1][1] == mark && Board[2][1] == mark)
		return 1;
	if (Board[0][2] == mark && Board[1][2] == mark && Board[2][2] == mark)
		return 1;
	// cheking diagonals
	if (Board[0][0] == mark && Board[1][1] == mark && Board[2][2] == mark)
		return 1;
	if (Board[0][2] == mark && Board[1][1] == mark && Board[2][0] == mark)
		return 1;

	return 0;
}


int main()
{
	int won = 0;
	int validInput = 0;


	for (int i=0; i<(BoardSize * BoardSize); i++)
	{
		system("cls");
		printBoard();
		if (turn) 
			cout << "Player 1 Turn (Symbol: O)" << endl;
		else
			cout << "Player 2 Turn (Symbol: X)" << endl;
		printInputMatrix();
		cout << "Enter Input from Input Matrix: ";
		cin >> input;
		while (input <= 0 || input > BoardSize * BoardSize)
		{
			cout << "Invalid Input. Please Re-Enter input: ";
			cin >> input;
		}

		if
			(turn) mark = 'O';
		else
			mark = 'X';



		validInput = addMark();
		if (!validInput)
		{
			i--;
			continue;
		}

		won = check();
		if (won)
		{
			system("cls");
			printBoard();
			if (turn)
				cout << endl << "*** Player 1 - You Won ***";
			else
				cout << endl << "*** Player 2 - You Won ***";
			break;
		}

		if (i == 8)
		{
			system("cls");
			printBoard();
			cout << endl << "*** MATCH DRAW ***";
		}

		turn = !turn;

	}

	return 0;
}