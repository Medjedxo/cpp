#include "Game.h"

Game::Game()
{
}

void Game::Run()
{
	Init();

	while (IsGameRuning())
	{
		Draw();
		Update();
	}

	Shutdown();
}
