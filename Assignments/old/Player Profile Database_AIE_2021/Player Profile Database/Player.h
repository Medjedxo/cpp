#pragma once


class Player
{
public:
	static const unsigned maxScore = 10000;
	static const unsigned int MaxNameLenght = 50;

	Player(unsigned int highscore = 0, const char* name = "");

	unsigned int GetHighScore() const { return highScore; }
	const char* GetName() const { return name; }

	void Draw();
	bool AskUser();

	bool operator<(const Player& other);

private:
	char name[MaxNameLenght] = {};
	unsigned int highScore = 0;
};

