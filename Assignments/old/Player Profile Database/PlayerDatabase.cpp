#include "PlayerDatabase.h"
#include <iostream>
#include <stdlib.h>
#include <string>
#include <algorithm>
#include "ioUtilities.h"

using namespace std;

void PlayerDatabase::Init()
{
	// Load leaderboard from file

	try 
	{
		leaderboard.AddPlayer("Liz", 10);
		leaderboard.AddPlayer("Steve", 15);
		leaderboard.AddPlayer("Simon", 99);
		leaderboard.AddPlayer("Alex", 88);

		leaderboard.SortByHighScore();
	}
	catch (exception& err)
	{
		cout << "Error:" << err.what() << endl;
	}
	
}

void PlayerDatabase::Shutdown()
{
	// Save the leaderboard to file
}

bool PlayerDatabase::IsGameRuning()
{
	return isGameRuning;
}

void PlayerDatabase::Update()
{
	displayMenu();
	string menuOption = getMenuOption();

	if (menuOption == "a")
	{
		addNewPlayer();
	}
	else if (menuOption == "m")
	{
		modifyPlayerByName();
	}
	else if (menuOption == "n")
	{
		leaderboard.SortByName();
	}
	/*else if (menuOption == "l")
	{
		leaderboard.Load();
	}
	else if (menuOption == "s")
	{
		leaderboard.Save();
	}
	else if (menuOption == "h")
	{
		leaderboard.Hack();
	}*/
	else if (menuOption == "c")
	{
		leaderboard.Clear();
	}
	else if (menuOption == "q")
	{
		isGameRuning = false;
	}
}

void PlayerDatabase::Draw()
{
	system("cls");
	leaderboard.Draw();
}

void PlayerDatabase::displayMenu()
{
	cout << endl << "--- Menu ---" << endl;
	cout << "A)dd player" << endl;
	cout << "M)odify player" << endl;
	cout << "N)ame sort" << endl;
	cout << "C)lear leaderboard" << endl;
	cout << "L)oad filedata" << endl;
	cout << "S)ave filedata" << endl;
	cout << "H)ack filedata" << endl;
	cout << "Q)uit" << endl;
	cout << "------------" << endl;
	cout << "> ";
}

string PlayerDatabase::getMenuOption()
{
	string userInput;
	CinClear();
	cin >> userInput;

	transform(userInput.begin(), userInput.end(), userInput.begin(), ::tolower); //Testing if this function transforms input into lower case
	return userInput;
}

void PlayerDatabase::addNewPlayer()
{
	if (!leaderboard.IsFull())
	{
		Player p;
		if (p.AskUser())
		{
			leaderboard.AddPlayer(p);

			leaderboard.SortByHighScore();
		}	
	}
	else
	{
		cout << " Leaderboard is full" << endl;
		int ch = getchar();
	}
}

void PlayerDatabase::modifyPlayerByIndex()
{
	CinClear();
	cout << "Enter  player position to modify> ";
	unsigned int pos;
	cin >> pos;

	if (pos < leaderboard.PlayersInUse())
	{
		leaderboard[pos].AskUser();

		leaderboard.SortByHighScore();
	}
}

void PlayerDatabase::modifyPlayerByName()
{
	CinClear();
	cout << "Enter name of player to modify> ";
	string name;
	cin >> name;

	unsigned int pos = 0;

	if (leaderboard.Search(name, pos))
	{
		leaderboard[pos].AskUser();

		leaderboard.SortByHighScore();
	}
}




