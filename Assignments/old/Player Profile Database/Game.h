#pragma once
#include "Leaderboard.h"


class Game
{
public:

	Game();

	// Runs main game loop
	void Run();


protected:

	// Pure virtual. Provides the algorithm to the Void Run();
	virtual void Init() = 0;
	virtual void Shutdown() = 0;
	virtual bool IsGameRuning() = 0;

	virtual void Update() = 0;
	virtual void Draw() = 0;
};

