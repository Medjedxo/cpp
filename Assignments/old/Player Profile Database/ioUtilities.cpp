#include "ioUtilities.h"
#include <iostream>

using namespace std;

void CinClear()
{
	cin.ignore(cin.rdbuf()->in_avail());
	cin.clear();
}
