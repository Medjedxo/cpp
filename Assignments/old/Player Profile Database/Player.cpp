#include "Player.h"
#include <string.h>
#include <iostream>
#include "ioUtilities.h"


using namespace std;


Player::Player(unsigned int highscore, const char* _name)
{
	strcpy_s(this->name, MaxNameLenght, _name);
	this->highScore = highscore;
}

void Player::Draw()
{
	cout << highScore << ", " << name << endl;
}

bool Player::AskUser()
{
	CinClear();

	cout << "Enter player name> ";
	cin >> name;
	cout << "Enter player score> ";
	cin >> highScore;

	if (highScore > maxScore)
		highScore = maxScore;

	return true;
}

bool Player::operator<(const Player& other)
{
	return (highScore < other.highScore);
}
