#include <iostream>
#include <Windows.h>
#include <random>
#include <time.h>

using namespace std;

const char* ESC = "\x1b";
const char* CSI = "\x1b[";

const char* TITLE = "\x1b[5;20H";
const char* INDENT = "\x1b[5C";
const char* YELLOW = "\x1b[93m";
const char* MAGENTA = "\x1b[95m";
const char* RESET_COLOR = "\x1b[0m";
const char* SAVE_CURSOR_POS = "\x1b[s";
const char* RESTORE_CURSOR_POS = "\x1b[u";



void ResetColor()
{
	cout << RESET_COLOR << endl;
}

// clear input buffer
void ClearBuffer()
{
	cin.clear();
	cin.ignore(cin.rdbuf()->in_avail());
	cin.get();
}

void main()
{
	const int EMPTY = 0;
	const int ENEMY = 1;
	const int TREASURE = 2;
	const int FOOD = 3;
	const int ENTRANCE = 4;
	const int EXIT = 5;
	const int MAX_RANDOM_TYPE = FOOD + 1;

	const int MAZE_WIDTH = 10;
	const int MAZE_HEIGHT = 6;

	// Set output mode to handle virtual terminal sequences
	DWORD dwMode = 0;
	HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
	GetConsoleMode(hOut, &dwMode);
	dwMode |= ENABLE_VIRTUAL_TERMINAL_PROCESSING;
	SetConsoleMode(hOut, dwMode);

	int height = 0;
	char firstLatterOfName = 0;
	int avatarHP = 0;

	// create a 2D array
	int rooms[MAZE_HEIGHT][MAZE_WIDTH];

	srand(time(nullptr));

	// fill the arrays with random room types
	for (int col = 0; col < MAZE_HEIGHT; col++)
	{
		for (int row = 0; row < MAZE_WIDTH; row++)
		{
			rooms[col][row] = rand() % MAX_RANDOM_TYPE;
		}
	}

	// sett the entrance and exit of the maze
	rooms[0][0] = ENTRANCE;
	rooms[MAZE_HEIGHT - 1][MAZE_WIDTH - 1] = EXIT;
		

	cout << TITLE << MAGENTA << "Welcome to ZORP!" << RESET_COLOR << endl;
	cout << INDENT << "ZORP is a game of advanture, danger, and low cunning." << endl;
	cout << INDENT << "It is definitely not related to any other text-based adventure game." << endl << endl;

	cout << INDENT << "First, some questions..." << endl;

	//save cursor position
	cout << SAVE_CURSOR_POS;


	// output the amp 
	cout << endl;
	cout << endl;
	cout << endl;
	cout << endl;
	for (int col = 0; col < MAZE_HEIGHT; col++)
	{
		cout << INDENT;
		for (int row = 0; row < MAZE_WIDTH; row++)
		{
			cout << "[ " << rooms[col][row] << " ]";
			
		}
		cout << endl;
	}

	// move the cursor back to the top of the map
	cout << RESTORE_CURSOR_POS;
	cout << INDENT << "How tall are you, in centimeters? " << INDENT << YELLOW;

	cin >> height;
	ResetColor();

	if (cin.fail())
		cout << INDENT << "You have failed the first challenge and are eaten by a grue.";
	else
		cout << INDENT << "You entered " << height;
	ClearBuffer();

	// move the cursor to the start of the 1st question 
	cout << RESTORE_CURSOR_POS;
	// delete the next 3 lines of text
	cout << CSI << "3M";
	// insert 3 lines (so map stays in same place)
	cout << CSI << "3L";
	cout << INDENT << "What is the first letter of your name? " << INDENT << YELLOW;

	cin.clear();
	cin.ignore(cin.rdbuf()->in_avail());
	cin >> firstLatterOfName;
	ResetColor();

	if (cin.fail() || isalpha(firstLatterOfName) == false)
		cout << INDENT << "You have failed the second challenge and re eaten by a grue.";
	else
		cout << INDENT << "You entered " << firstLatterOfName;
	ClearBuffer();
	
	// move the cursor to the start of the 1st question, then up 1,
	// then delete and insert 4 lines
	cout << RESTORE_CURSOR_POS << CSI << "A" << CSI << "4M" << CSI << "4L";

	if (firstLatterOfName != 0)
		avatarHP = (float)height / (firstLatterOfName * 0.02f);
	else
		avatarHP = 0;
	
	cout << INDENT << "Using a complex deterministic algorithm it has been calculated that you have " << avatarHP << " hit point(s)." << endl;

	cout << endl << INDENT << "Press 'Enter' to exit the program.";
	ClearBuffer();
}