#include <iostream>
#include <string>

using namespace std;

struct rectangle
{
	float botRight = 0;
	float botLeft = 0;
	float topRight = 0;
	float topLeft = 0;

	float RGBA = 0;
};

struct Player 
{
	string name = "";
	int health = 0;
	int score = 0;

	float position = 0;
	float velocity = 0;

	string GetName() const { return name; }
	int GetScore() const { return score; }

	void SetScore(int Score)
	{
		score = Score;
	}

	void SetName(string Name)
	{
		name = Name;
	}

	void GetPlayerInfo(int index)
	{

		cout << "please enter players name: " << endl;
		cin >> name;
		cout << "Please enter players score: " << endl;
		cin >> score;

	}
};

const unsigned int NumberOfPlayers = 3;
Player Players[NumberOfPlayers];


void GetPlayerInfo(int index)
{
	string name;
	int score;
	cout << "Player " << index << " Name:";
	cin >> name;
	cout << "Player " << index << " Score:";
	cin >> score;

	Players[index].SetName(name);
	Players[index].SetScore(score);
	
}

void Start()
{
	for (unsigned int i = 0; i < NumberOfPlayers; i++)
	{
		GetPlayerInfo(i);
	}
}

void PrintPlayersNames()
{
	for (unsigned int i = 0; i < NumberOfPlayers; i++)
	{
		cout << " Player " << i << " Name is: ";
		cout << Players[i].name << endl;
	}
}

void FindMatch()
{
	string name;
	int score;
	bool exist = false;

	cout << "Please enter players name you wish to find: " << endl;
	cin >> name;
	
	for (unsigned int i = 0; i < NumberOfPlayers; i++)
	{
		if (name == Players[i].name)
		{
			score = Players[i].score;
			cout << "players " << name << " Score is: " << score << endl;
			exist = true;
			break;
		}			
	}	
	while (!exist)
	{
		cout << "No Match Found" << endl;
		break;
	}
}



int main() 
{
	Start();
	FindMatch();
	
		
	return 0;
}