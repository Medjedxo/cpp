#pragma once
#include <vector>
#include "Zergling.h"
#include "Marine.h"

bool marineAlive(std::vector<Marine> squad);


// Is there a zergling Alive
bool zerglingAlive(std::vector<Zergling> swarm);

