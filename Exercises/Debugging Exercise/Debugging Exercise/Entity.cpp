#include "Entity.h"



Entity::Entity(int health)
	: health(health)
{
}


Entity::~Entity()
{
}

void Entity::takeDamage(int damage)
{
	if (damage > health)
		health = 0;
	else
		health -= damage;
}

bool Entity::isAlive()
{
	return (health > 0);
}
