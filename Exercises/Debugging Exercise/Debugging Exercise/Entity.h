#pragma once
class Entity
{
public:
	Entity(int health);
	~Entity();

	virtual int attack() = 0;
	virtual void takeDamage(int damage);

	bool isAlive();

private:
	unsigned int health;
};

