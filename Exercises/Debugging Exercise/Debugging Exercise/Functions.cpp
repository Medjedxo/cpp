#include "Functions.h"
#include <vector>
#include "Marine.h"

bool marineAlive(std::vector<Marine> squad)
{
	return !squad.empty();
}

// Is there a zergling Alive
bool zerglingAlive(std::vector<Zergling> swarm)
{
	return !swarm.empty();
}