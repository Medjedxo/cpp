#include <iostream>
#include <limits>

using namespace std;

void RevString(char* str)
{
	int len = strlen(str);
	for (int i = 0; i < len / 2; i++)
	{
		swap(str[i], str[len - i - 1]);
	}
}

int CountEven(int* array, int array_len)
{
	unsigned int counter = 0;
	for (int i = 0; i < array_len; i++)
	{
		if (array[i] % 2 == 0)
			counter++;
	}
	return counter;
}

double* functionMaximum(double* array, int array_size)
{
	double* p = nullptr;
	double currentMax = numeric_limits<double>::min();

	for (int i = 0; i < array_size; i++)
	{
		if (array[i] > currentMax)
		{
			currentMax = array[i];
			p = &array[i];
		}
	}

	return p;
}

bool Contains(char* array, char search_value)
{
	for (int i = 0; i < CHAR_MAX; i++)
	{
		if (array[i] == search_value)
		{
			return true;
		}
	}
	return false;
}




void main()
{
	// c = 6940, d = 9772 and e = 2224
	char c = 'T', d = 'S';
	char *p1 = &c; // currently equal to 6940
	char *p2 = &d; // currently equal to 9772
	char *p3; //euqals to nothing, just a newly made pointer, hold nothing (null)

	p3 = &d; // was set to point of the memorry adress of d, which is equal to 9772

	cout << "*p3 = " << *p3 << endl; // asks to print THE VALUE inside the memory point. Currently pointing to memory point 9772 which holds the variable (which contain 'd'.. this will print => 'S'.

	p3 = p1; // now we ask p3 to point the same memory slot as p1 which is &c hence equal to 6940

	cout << "*p3 = " << *p3; // asks to print THE VALUE inside the memory point. Currently pointing to memory point 6940 which holds the variable (which contain 'c'.. this will print => 'T'.
	cout << "*, p3 = " << p3 << endl; //asks for THE POINT not the inside value, so this should print 6940

	*p1 = *p2; //even with added stars this still asks a point to be pointing to a diffrent memory slot. In this case the 'd' which is 9772

	cout << "*p1 = " << *p1; // should print 'S'
	cout << ", p1 = " << p1 << endl; //asks for THE POINT not the inside value, so this should print 9772



	// consider the following statments:
	int *p;
	int i;
	int k;

	i = 42; // i = 42
	k = i; // k = 42
	p = &i; //points at the memory slot of 'i' ans asks for whats inside (which is 'i') => currently containing 42.

	// After these statements, which of the following statements will change the value of i to 75?
	// A. k = 75
	// B. *k = 75;
	// C. p = 75;
	// D. *p = 75;
	// E. Two or more of the answers will change i to 75.
	//
	// Anwser:  D theis will ask the point (which currently pointing to 'i' memory slot trough &i) to change its value to 75.





	// Explain the error.
	// char c = 'A';
	// double *p = &c;

	// Anwser: We asking the compailer to have a 'double' point contain a 'char' data in it.
	// This can be solved by changing the 'double' to 'char' ===> char *p = &c;    or maybe go trou convertion. 





	// Look at the following code. Give the value of the left-hand side variable in each assignment statement. Assume the lines are executed sequentially. Assume the address of the blocks array is 4434.
	
	char blocks[3] = { 'A','B','C' }; // blocks[0] = 4434, blocks[1] = 4435 and blocks[2] = 4436
	char* ptr = &blocks[0]; // currently equal to 4434
	char temp; // currently null
	
	temp = blocks[0]; // equal to 'A'     
	temp = *(blocks + 2); // equal to 'C'      
	temp = *(ptr + 1);  // qual to 'B'
	temp = *ptr; // equal to 'A'
	
	ptr = blocks + 1; // equal to 4435      this will change the point to always be at [1] which is 'B'/4435
	temp = *ptr; // equal to 'B' 
	temp = *(ptr + 1); // euqal to 'C'
	
	ptr = blocks; // sets ptr back to blocks[0] which is 'A'
	temp = *++ptr; // asks the pointer to be increased by 1 and then assign it's value to him making it equal to blocks[1] which is 'B'
	temp = ++ *ptr; // asks the pointer position, then increase it by one, seetin itself to blocks[2] which is 'C' BUT also increases the value inside pointer by one making it block[1] = 'C'
	temp = *ptr++; // asks the pointers to increase its position by 1 and give him the value which is blocks[2] or 'C' and then move the pointer by 1 to block[2] 'C'
	temp = *ptr; //pointer still equals to Blocks[2] or 'C' since we didn't increase it's value in the last 2 questions.
	
	





	/*
	// Write a function for each of the following descriptions. For each function, use the pointer notation ONLY. Do NOT use the array index [] notation.
	    A.Write a function RevString(char* array)which reverses array. The function returns nothing. 
		B.Write a function CountEven(int* array, int array_len)which receives an integer array and its size, and returns the number of even numbers in the array. 
		C.Write a functionMaximum(double* array, int array_size)that returns a pointer to the maximum value of an array of doubles. If the array is empty, return nullptr.
		D.Write a function Contains(char* array, char search_value)which returns true if the 1st parameter contains the 2nd parameter, or false otherwise.
	*/

	void RevString(char* str);
	
	int CountEven(int* array, int array_len);

	int functionMaximum(double* array, int array_size);

	bool Contains(char* array, char search_value);
	


	
	// Predict the output of the following program. Run the programto see if your prediction is right.

	/*
	  int* ptr_a, ptr_b, * ptr_c; 
	  
	  ptr_a = new int; // equals 3
	  *ptr_a = 3; // equals 3
	  ptr_b = ptr_a; cout << *ptr_a << " " << *ptr_b << "\n"; //returns 3 and 3 
	  
	  ptr_b = new int; //equals to something random behind the scenes
	  *ptr_b = 9; / euqals 9
	  cout << *ptr_a << " " << *ptr_b << "\n"; // prints 3 and 9
	  
	  *ptr_b = *ptr_a; //equals 3
	  cout << *ptr_a << " " << *ptr_b << "\n";  // prints 3 and 3
	  
	  delete ptr_a; 
	  ptr_a = ptr_b; 
	  cout << *ptr_a << " " << *&*&*&*&*ptr_b << "\n"; 
	  
	  ptr_c = &ptr_a; 
	  cout << *ptr_c << " " << **ptr_c << "\n"; 
	  
	  delete ptr_a; 
	  ptr_a = NULL;
	*/


}




