#include <string>
#include "zombie.h"

using namespace std;


void main()
{
	Zombie zombie1, zombie2;

	zombie2.SetName("Steve");
	zombie1.SetName("Alex");

	zombie1.Attack(zombie2);
	zombie2.Attack(zombie1);
	zombie1.Attack(zombie2);
}


