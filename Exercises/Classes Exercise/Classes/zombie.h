#include <string>
#include <iostream>

using namespace std;


class Zombie
{
private:
	int health = 100;
	int attack = 30 ;
	string name;
	string previousOccupation = "";

public:
	int GetHealth()
	{
		return health;
	}
	int GetAttack()
	{
		return attack;
	}
	string GetOccupation()
	{
		return previousOccupation;
	}
	string GetName()
	{
		name = "Bob";
		return name;
	}
	void SetHealth(int h)
	{
		health = h;
	}
	void SetAttack(int a)
	{
		attack = a;
	}
	void SetOccupation(string o)
	{
		previousOccupation = o;
	}
	void SetName(string n)
	{
		name = n;
	}
	void Attack(Zombie)
	{
		int damage;
		bool dead = (health <= 0);
		while (!dead)
		{
			damage = attack - health;
			cout << "Zombie took " << attack << " damage!";
			health = damage;
			break;
		}
		while (dead)
		{
			cout << "Looks like " << name << " just died.... good job?";
			break;
		}
	}
};