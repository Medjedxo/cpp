#include <iostream>
#include <fstream>
#include <vector>
#include <string>

using namespace std;

int main()
{
	fstream MyLog;
	string data;
	string answer;

	cout << "Welcome to your log! What would you like to do? \n1.)Enter Data \n2.)clear (*WARNING*this will delete all of your current data stored in the log)" << endl;
	cin >> answer;

	while (answer == "clear")
	{
		remove("MyLog.txt");
	}
	
	

	MyLog.open("MyLog.txt", ios::out | ios::app);
	if (MyLog.is_open())
	{
		cout << "Please enter your log: " << endl;
		std::getline(cin, data, '\n');
		MyLog << data << " ";
		
		MyLog.close();
	}
	else
		cout << "Something went wrong" << endl;
	

	
	cin.get();
	return 0;
}